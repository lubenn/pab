var pab_cart_expiration_sent = false;

(function ($) {
  Drupal.behaviors.pab_cart_expiration = {
    attach: function (context, settings) {
      if (context == document) {
        var expire = settings.commerce_cart_expiration.expire_in;
        var url_ajax = settings.commerce_cart_expiration.url_ajax;
        var url_redirect = settings.commerce_cart_expiration.url_redirect;

        // Get all cart expiration blocks.
        // var blocks = $('.block-commerce-cart-expiration');

        // blocks.each(function(index, block) {
          commerceCartExpirationTick($(this), expire, url_ajax, url_redirect);
        // });
      }
    }
  };

  /**
   * Refreshes the expiration time.
   */
  function commerceCartExpirationTick(block, expire, url_ajax, url_redirect) {
    if (expire <= 0) {
      commerceCartExpirationExpired(url_ajax, url_redirect);
      return;
    }
    expire--;
    console.log(expire);
    setTimeout(function() {
      commerceCartExpirationTick(block, expire, url_ajax, url_redirect);
    }, 1000);
  }

  /**
   * Calls drupal and tries to delete the order.
   */
  function commerceCartExpirationExpired(url_ajax, url_redirect) {
    if (!pab_cart_expiration_sent) {
      $.ajax({
        url: url_ajax,
        type: 'GET',
        success: function(response) {
          if (response.status) {
            window.location = url_redirect;
          }
        },
      });
      pab_cart_expiration_sent = true;
    }
  }
})(jQuery);
