<?php

function pab_booking_modal($entity_id, $event_type, $event_id, $start_date, $end_date) {
  ctools_include('modal');
  // If any info missing we cannot load the event.
  if ($event_id == NULL || $start_date === 0 || $end_date === 0) {
    $output[] = ctools_modal_command_dismiss();
    drupal_set_message(t('Unable to load event.'), 'error');
  }

  $modal_content = array();
  $modal_content['title'] = t('Manage Product');
  $modal_content['content'] = '';
  if($event_id > 0) {
    // load booking show booking info
    if($event_type->status == 2) {
      if($booking = pab_booking_load($event_id)) {
        $order = commerce_order_load($booking->order_id);
        $view = entity_view('commerce_order', array($order), 'modal');
        $modal_content['content'] = render($view);
      }
    }
  } else {
    $modal_content['content'] = drupal_get_form('pab_booking_modal_event_manager_form', $entity_id, $event_type, $event_id, $start_date, $end_date);
  }

  if (isset($modal_content['title']) && isset($modal_content['content'])) {
    ctools_modal_render($modal_content['title'], $modal_content['content']);
  } elseif (isset($modal_content['commands'])) {
    print ajax_render($modal_content['commands']);
    exit();
  }

}

/**
 * The Event Manager Form.
 */
function pab_booking_modal_event_manager_form($form, &$form_state, $entity_id, $event_type, $event_id, $start_date, $end_date) {
  module_load_include('inc', 'pab', 'includes/pab.booking');

  $form = array();
  $new_event_id = $event_id;

  if (isset($form_state['values']['change_event_status'])) {
    $new_event_id = $form_state['values']['change_event_status'];
  }

  $form['#attributes']['class'][] = 'bat-management-form bat-event-form';

  $form['#prefix'] = '<div id="replace_textfield_div">';
  $form['#suffix'] = '</div>';

  $form['entity_id'] = array(
    '#type' => 'hidden',
    '#value' => $entity_id,
  );

  $form['event_type'] = array(
    '#type' => 'hidden',
    '#value' => $event_type->type,
  );

  $form['event_id'] = array(
    '#type' => 'hidden',
    '#value' => $event_id,
  );

  $form['bat_start_date'] = array(
    '#type' => 'hidden',
    '#value' => $start_date,
  );

  $form['bat_end_date'] = array(
    '#type' => 'hidden',
    '#value' => $end_date,
  );

  $unit = entity_load_single($event_type->target_entity_type, $entity_id);

  $date_format = variable_get('bat_date_format', 'Y-m-d H:i');
  $form['event_details'] = array(
    '#prefix' => '<div class="event-details">',
    '#markup' => t('Dates: @startdate to @enddate', array('@startdate' => $start_date->format($date_format), '@enddate' => $end_date->format($date_format))),
    '#suffix' => '</div>',
  );

  $state_options = bat_unit_state_options($event_type->type);

  $form['change_event_status'] = array(
    '#title' => t('Change the state to') . ': ',
    '#type' => 'select',
    '#options' => $state_options,
    '#ajax' => array(
      'callback' => 'pab_booking_modal_event_status_change',
      'wrapper' => 'replace_textfield_div',
    ),
    '#empty_option' => t('- Select -'),
  );

  return $form;
}

/**
 * The callback for the change_event_status widget of the event manager form.
 */
function pab_booking_modal_event_status_change($form, &$form_state) {
  global $user;

  $start_date = $form_state['values']['bat_start_date'];
  $end_date = $form_state['values']['bat_end_date'];
  $entity_id = $form_state['values']['entity_id'];
  $event_id = $form_state['values']['event_id'];
  $event_type = $form_state['values']['event_type'];
  $state_id = $form_state['values']['change_event_status'];

  if($form_state['values']['change_event_status'] == 3) {
    $product = $form_state['values']['entity_id'];

    $start_date = new DateTime($start_date->date);
    $start_date = $start_date->format('Y-m-d');

    $end_date = new DateTime($end_date->date);
    $end_date->sub(new DateInterval('PT1M'));
    $end_date = $end_date->format('Y-m-d');
    $q = drupal_http_build_query(array('start_date' => 'now', 'end_date' => 'then', 'product' => $product));
    ctools_include('modal');
    ctools_include('ajax');
    $commands = array();
    $commands[] = ctools_modal_command_dismiss();
    $commands[] = ctools_ajax_command_redirect('/admin/pab/bookings/add', 0, array('query'=> array('start_date' => $start_date, 'end_date' => $end_date, 'product' => $product)));

    print ajax_render($commands);
    exit;
  }

  $event = bat_event_create(array('type' => $event_type));
  $event->created = REQUEST_TIME;
  $event->uid = $user->uid;

  $event->start_date = $start_date->format('Y-m-d H:i');
  $end_date->sub(new DateInterval('PT1M'));
  $event->end_date = $end_date->format('Y-m-d H:i');

  $event_type_entity = bat_event_type_load($event_type);
  // Construct target entity reference field name using this event type's target entity type.
  $target_field_name = 'event_' . $event_type_entity->target_entity_type . '_reference';
  $event->{$target_field_name}[LANGUAGE_NONE][0]['target_id'] = $entity_id;

  $event->event_state_reference[LANGUAGE_NONE][0]['state_id'] = $state_id;

  $event->save();

  $state_options = bat_unit_state_options($event_type);
  $form['form_wrapper_bottom'] = array(
    '#prefix' => '<div>',
    '#markup' => t('Changed state to <strong>@state</strong>.', array('@state' => $state_options[$state_id])),
    '#suffix' => '</div>',
    '#weight' => 9,
  );

  return $form;
}
