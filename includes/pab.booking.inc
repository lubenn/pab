<?php

function pab_view_booking($order) {
  $view = entity_view('commerce_order', array($order), 'full', null, TRUE);
  return $view;
}

function pab_booking_form() {
  $form = drupal_get_form('pab_booking_edit_form', FALSE);
  return $form;
}

function pab_booking_edit_form($form, &$form_state, $order) {
  drupal_set_title(t('Manage Booking'));

  $form = array();
  $units = array();
  $event = FALSE;
  $delivery = 0;
  $products_orders = array();

  if($order) {
    $line_items = $order->commerce_line_items[LANGUAGE_NONE];
    $key=0;
    foreach($line_items as $line_item) {
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item['line_item_id']);
      if($line_item_wrapper->type->value() == 'product') {
        $event = $line_item_wrapper->field_events_reference->value();
        $products_orders[$key] = array();
        $products_orders[$key]['amount'] = $line_item_wrapper->commerce_total->amount->value() / 100;
        $products_orders[$key]['type_id'] = bat_unit_load($event->event_bat_unit_reference[LANGUAGE_NONE][0]['target_id'])->type_id;
        $products_orders[$key]['status'] = $event->event_state_reference[LANGUAGE_NONE][0]['state_id'];
        $products_orders[$key]['event_id'] = $event->event_id;
        $products_orders[$key]['line_item'] = $line_item['line_item_id'];
        $key++;
      }
      if($line_item_wrapper->type->value() == 'shipping') {
        $delivery = $line_item_wrapper->commerce_total->amount->value() / 100;
      }
    }
  }

  if (!isset($form_state['num_products'])) {
    $form_state['num_products'] = count($products_orders) > 0 ? count($products_orders) : 1;
  }

  $notes =  '';
  $start_date = $event ? $event->start_date : '';
  $end_date =   $event ? $event->end_date : '';
  $notes = isset($order->field_order_notes[LANGUAGE_NONE]) ? $order->field_order_notes[LANGUAGE_NONE][0]['value'] : '';
  $profile = $order && isset($order->commerce_customer_billing[LANGUAGE_NONE]) ? commerce_customer_profile_load($order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id']) : commerce_customer_profile_new('billing');
  $email = $order ? $order->mail : '';
  $products = array();
  $types = bat_unit_get_types();
  $default_currency = commerce_currency_load();

  foreach($types as $type) {
    $products[$type->type_id] = $type->name;
  }

  $start_date_id = drupal_html_id('datepicker-start-date');
  $start_date_selector = '#' . $start_date_id . ' .form-text';

  $end_date_id = drupal_html_id('datepicker-end-date');
  $end_date_selector = '#' . $start_date_id . ' .form-text';

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => array('completed' => t('Booked'), 'canceled' => t('Cancelled')),
    '#description' => t('Setting as cancelled will cancel the booking and allow the product to be booked again on these dates.'),
    '#default_value' => $order ? $order->status : 'completed',
    '#required' => TRUE,
  );

  $form['booking'] = array(
    '#type' => 'value',
    '#value' => $products_orders
  );

  $form['start_date'] = array(
    '#type' => 'date_popup',
    '#description' => '',
    '#title' => t('Drop off Date'),
    '#date_format' => 'l, F d, Y',
    '#default_value' => $start_date,
    '#date_label_position' => 'none',
    '#datepicker_options' => array('endDateSelector' => $end_date_selector, 'minDate' => '0'),
    '#required' => TRUE,
    '#prefix' => '<div class="start-date" id="' . $start_date_id . '">',
    '#suffix' => '</div>',
  );

  $form['end_date'] = array(
    '#type' => 'date_popup',
    '#description' => '',
    '#title' => t('Pick up Date'),
    '#date_format' => 'l, F d, Y',
    '#default_value' => $end_date,
    '#date_label_position' => 'none',
    '#datepicker_options' => array('startDateSelector' => $start_date_selector, 'minDate' => '0'),
    '#required' => TRUE,
    '#prefix' => '<div class="end-date" id="' . $end_date_id . '">',
    '#suffix' => '</div>',
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'pab') . '/js/pab_date_popup.js',
        array(
          'data' => array(
            'pab' => array(
              'pabBookingStartDay' => '+1 day',
              'pabDateFormat' => 'l, F d, Y',
              'datepickers' => array(
                $start_date_selector => array(
                  'endDateSelector' => $end_date_selector,
                ),
              ),
            ),
          ),
          'type' => 'setting',
        ),
      ),
    ),
  );

  if(module_exists('commerce_shipping')) {
    $form['delivery'] = array(
      '#type' => 'textfield',
      '#title' => t('Delivery Charge'),
      '#input_group' => TRUE,
      '#field_prefix' => '',
      '#field_suffix' => $default_currency['code'],
      '#required' => TRUE,
      '#description' => t('The delivery charge.'),
      '#default_value' => $delivery,
    );
  }

  $form['products'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#prefix' => '<div id="product_wrapper">',
    '#suffix' => '</div>',
  );

  for ($i = 0; $i < $form_state['num_products']; $i++) {
    $form['products']['items'][$i] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => isset($products_orders[$i]) ? TRUE : FALSE,
      '#title' => t('Product')
    );
    $form['products']['items'][$i]['value'] = array(
      '#type' => 'value',
      '#value' => isset($products_orders[$i]) ? $products_orders[$i] : FALSE,
    );
    $form['products']['items'][$i]['status'] = array(
      '#type' => 'select',
      '#title' => t('Status'),
      '#options' => array(3 => t('Booked'), 1 => t('Cancelled')),
      '#description' => t('Setting as cancelled will allow the product to be booked again on these dates.'),
      '#default_value' => isset($products_orders[$i]) ? $products_orders[$i]['status'] : 3,
      '#required' => TRUE,
    );
    $form['products']['items'][$i]['product'] = array(
      '#type' => 'select',
      '#title' => t('Product'),
      '#options' => $products,
      '#required' => TRUE,
      '#default_value' => isset($products_orders[$i]) ? $products_orders[$i]['type_id'] : '',
    );
    $form['products']['items'][$i]['price'] = array(
      '#type' => 'textfield',
      '#title' => t('Price Paid'),
      '#input_group' => TRUE,
      '#field_prefix' => '',
      '#field_suffix' => $default_currency['code'],
      '#required' => TRUE,
      '#description' => t('The total charge for the product.'),
      '#default_value' => isset($products_orders[$i]) ? $products_orders[$i]['amount'] : '',
    );
  }

  $form['products']['add_page'] = array(
    '#type' => 'submit',
    '#value' => t('Add a product'),
    '#submit' => array('bat_booking_add_another'),
    '#ajax' => array(
      'callback' => 'bat_booking_add_another_callback',
      'wrapper' => 'product_wrapper',
    ),
    '#limit_validation_errors' => array(),
  );

  $form['notes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notes'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 48,
  );

  $form['notes']['notes'] = array(
    '#type' => 'textarea',
    '#title' => t('Notes'),
    '#default_value' => $notes,
  );

  $form_state['customer_profile'] = $profile;

  if(!$order) {
    $order = commerce_order_new(0, 'completed');
  }

  $form['questions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Questions'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => FALSE,
    '#weight' => 48,
  );

  field_attach_form('commerce_order', $order, $form['questions'], $form_state, $langcode = NULL);

  foreach (element_children($form['questions']) as $child) {
    if(strpos($child, 'sf_') === FALSE) {
      unset($form['questions'][$child]);
      continue;
    }
    $form['questions']['#access'] = TRUE;
  }

  $form['customer_profile_billing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Billing Information'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 48,
    '#tree' => TRUE,
    '#parents' => array('customer_profile_billing')
  );

  field_attach_form('commerce_customer_profile', $profile, $form['customer_profile_billing'], $form_state);

  $form['customer_profile_billing']['commerce_customer_address'][LANGUAGE_NONE][0]['#type'] = 'container';

  $form['customer_profile_billing']['commerce_customer_address'][LANGUAGE_NONE][0]['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#default_value' => $email,
    '#weight' => 100,
  );

  $form_state['billing_profile'] = $profile;

  if(module_exists('commerce_shipping')) {
    $form['customer_profile_shipping'] = array(
      '#type' => 'fieldset',
      '#title' => t('Delivery Information'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      '#parents' => array('customer_profile_shipping'),
      '#weight' => 49,
    );
    $shipping_profile = $order &&
    isset($order->commerce_customer_shipping[LANGUAGE_NONE]) ? commerce_customer_profile_load($order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id']) :
    commerce_customer_profile_new('shipping');
    $form_state['shipping_profile'] = $shipping_profile;
    field_attach_form('commerce_customer_profile', $shipping_profile, $form['customer_profile_shipping'], $form_state);

    $form['customer_profile_shipping']['commerce_customer_address'][LANGUAGE_NONE][0]['#type'] = 'container';
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 50
  );

  if($order->order_id) {
    $form['delete']['#markup'] = l(t('Delete'), url('admin/pab/bookings/'.$order->order_id.'/delete'), array('attributes' => array('class' => array('btn', 'btn-danger'))));
    $form['delete']['#weight'] = 51;
  }

  return $form;
}

function bat_booking_add_another($form, &$form_state) {
  $form_state['num_products']++;
  $form_state['rebuild'] = TRUE;
}

function bat_booking_add_another_callback($form, $form_state) {
  return $form['products'];
}

function pab_booking_edit_form_validate($form, &$form_state) {
  if(!empty($form_state['values']['customer_profile_billing']['commerce_customer_address'][LANGUAGE_NONE][0]['email'])) {
    if(!valid_email_address($form_state['values']['customer_profile_billing']['commerce_customer_address'][LANGUAGE_NONE][0]['email'])) {
      form_set_error('', t('A valid email address is required. Alternatively leave this field blank.'));
    }
  }

  if(isset($form_state['values']['start_date']['date'])) {
    return;
  }
  if(isset($form_state['values']['end_date']['date'])) {
    return;
  }

  if($form_state['values']['status'] == 'canceled') {
    return;
  }

  $dates = pab_format_dates($form_state['values']['start_date'], $form_state['values']['end_date']);
  // need to check each products dates
  foreach($form_state['values']['products']['items'] as $key => $product) {
    if($product['product'] == '_none') {
      continue;
    }
    if($product['status'] == 1) {
      continue;
    }
    $type = bat_type_load($product['product']);
    $drupal_units = bat_unit_load_multiple(FALSE, array('type_id' => $type->type_id));
    $unit = reset($drupal_units);
    $product_event_id = false;
    if($product['value']) {
      $product_event_id = $product['value']['event_id'];
    }
    if($product_event_id && $unit) {
      if($events = pab_calendar_events($type, $dates['start_date'], $dates['end_date'])) {
        foreach ($events[$unit->unit_id] as $blocking_event) {
          $event_id = $blocking_event->getValue();
          if ($event_id != $product_event_id) {
            if ($n_event = bat_event_load($event_id)) {
              $state = bat_event_load_state($n_event->event_state_reference[LANGUAGE_NONE][0]['state_id']);
              if ($state['blocking']) {
                form_set_error('', t('The selected dates are not available for @product', array('@product' => $unit->name)));
              }
            }
          }
        }
      }
    } else {
      if($errors = module_invoke_all('pab_availability_restrictions', $dates['start_date'], $dates['end_date'], $type)) {
        form_set_error('', t('The selected dates are not available for @product', array('@product' => $unit->name)));
      }
    }
  }

  return;
}

function pab_booking_edit_form_submit($form, &$form_state) {

  $new = TRUE;
  $line_items_to_delete = array();

  if(!empty($form_state['build_info']['args'][0])) {
    $new = FALSE;
    $order = $form_state['build_info']['args'][0];
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  }

  $dates = pab_format_dates($form_state['values']['start_date'], $form_state['values']['end_date']);
  $default_currency = commerce_currency_load();

  if($new) {
    // Create a new order
    $order = commerce_order_new(0, 'completed');
    commerce_order_save($order);
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

    foreach($form_state['values']['products']['items'] as $key => $product) {
      if($product['product'] == '_none') {
        continue;
      }

      $new_product = new StdClass();
      $new_product->price     = $product['price'] * 100;
      $new_product->type      = bat_type_load($product['product']);
      $new_product->product   = commerce_product_load($new_product->type->field_product[LANGUAGE_NONE][0]['product_id']);
      $new_product->status    = $product['status'];
      $new_product->order_id  = $order->order_id;
      $new_product->is_new    = TRUE;
      if($form_state['values']['status'] == 'canceled') {
        $new_product->status = 1;
      }
      $units = bat_unit_load_multiple(FALSE, array('type_id' => $new_product->type->type_id));
      $new_product->unit = reset($units);
      $new_product->event     = pab_booking_create_event($new_product, $dates);
      $new_product->line_item = pab_booking_create_line_item($new_product, $dates);
      $order_wrapper->commerce_line_items[] = $new_product->line_item;
    }

    $form_state['line_items'] = array(
      '#type' => 'value',
      '#value' => $order_wrapper->commerce_line_items->value()
    );

    if(module_exists('commerce_shipping')) {
      // Shipping
      $unit_price = array(
        'amount' => commerce_currency_decimal_to_amount($form_state['values']['delivery'], $default_currency['code']),
        'currency_code' => $default_currency['code'],
        'data' => array(),
      );
      $line_item = commerce_shipping_line_item_new('pab_shipping_service', $unit_price, $order->order_id);
      commerce_line_item_rebase_unit_price($line_item);
      commerce_line_item_save($line_item);
      $order_wrapper->commerce_line_items[] = $line_item;
    }

    drupal_set_message(t('Booking created'));
  } else {

    $events = array();
    $line_items = array();
    foreach($form_state['values']['products']['items'] as $key => $product) {
      $new_product = new StdClass();
      $new_product->price     = $product['price'] * 100;
      $new_product->type      = bat_type_load($product['product']);
      $new_product->product   = commerce_product_load($new_product->type->field_product[LANGUAGE_NONE][0]['product_id']);
      $new_product->status    = $product['status'];
      $new_product->order_id  = $order->order_id;
      $new_product->is_new    = TRUE;
      if($form_state['values']['status'] == 'canceled') {
        $new_product->status = 1;
      }
      $units = bat_unit_load_multiple(FALSE, array('type_id' => $new_product->type->type_id));
      $new_product->unit = reset($units);

      if(!empty($product['value'])) {
        $new_product->is_new = FALSE;
        $new_product->event  = bat_event_load($product['value']['event_id']);
        $new_product->line_item = commerce_line_item_load($product['value']['line_item']);
      }

      if($new_product->is_new) {
        $new_product->event     = pab_booking_create_event($new_product, $dates);
        $new_product->line_item = pab_booking_create_line_item($new_product, $dates);
      }

      if(!$new_product->is_new) {
        $price   = $new_product->line_item->commerce_total[LANGUAGE_NONE][0]['amount'];
        $product = $new_product->line_item->field_bat_type[LANGUAGE_NONE][0]['target_id'];
        $status =  $new_product->event->event_state_reference[LANGUAGE_NONE][0]['state_id'];
        $dates_changed = FALSE;
        if($new_product->event->start_date != $dates['start_date']) {
          $dates_changed = TRUE;
        }
        if($new_product->event->end_date != $dates['end_date']) {
          $dates_changed = TRUE;
        }
        // if product, price, or status different update
        if($new_product->price != $price OR $new_product->type->type_id != $product OR $new_product->status != $status OR $dates_changed) {

          $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $new_product->line_item->line_item_id);
          $line_item_wrapper->field_start_date->set(strtotime($dates['start_date']));
          $line_item_wrapper->field_end_date->set(strtotime($dates['end_date']));
          $line_item_wrapper->field_bat_type->set($new_product->type->type_id);
          $line_item_wrapper->commerce_unit_price->amount = $new_product->price;
          commerce_line_item_rebase_unit_price($line_item_wrapper->value());
          commerce_line_item_save($new_product->line_item);

          // set event
          $new_product->event->event_state_reference[LANGUAGE_NONE][0]['state_id'] = $new_product->status;
          $new_product->event->start_date = $dates['start_date'];
          $new_product->event->end_date = $dates['end_date'];
          $new_product->event->event_bat_unit_reference[LANGUAGE_NONE][0]['target_id'] = $new_product->unit->unit_id;
          $new_product->event->save();
        }
      }
      $events[] = $new_product->event->event_id;
      $line_items[] = $new_product->line_item->line_item_id;
    }

    if(module_exists('commerce_shipping')) {
      foreach($order_wrapper->commerce_line_items->value() as $shipping_line_item) {
        if($shipping_line_item->type != 'shipping') {
          continue;
        }

        $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $shipping_line_item);
        $line_item_wrapper->commerce_unit_price->amount = $form_state['values']['delivery'] * 100;
        commerce_line_item_rebase_unit_price($shipping_line_item);
        commerce_line_item_save($shipping_line_item);
        $line_items[] = $shipping_line_item->line_item_id;
      }
    }

    $order_wrapper->commerce_line_items->set($line_items);

    $form_state['line_items'] = array(
      '#type' => 'value',
      '#value' => $order_wrapper->commerce_line_items->value()
    );

    drupal_set_message(t('Booking updated'));
  }

  $order_wrapper->field_order_notes->set($form_state['values']['notes']);
  $order_wrapper->status->set($form_state['values']['status']);
  if(!empty($form_state['values']['customer_profile_billing']['commerce_customer_address'][LANGUAGE_NONE][0]['email'])) {
    $order_wrapper->mail->set($form_state['values']['customer_profile_billing']['commerce_customer_address'][LANGUAGE_NONE][0]['email']);
  }

  if(module_exists('commerce_shipping')) {
    $shipping_profile = $form_state['shipping_profile'];
    $shipping_profile->status = TRUE;
    field_attach_submit('commerce_customer_profile', $shipping_profile, $form['customer_profile_shipping'], $form_state);
    commerce_customer_profile_save($shipping_profile);
    $order_wrapper->commerce_customer_shipping = $shipping_profile;
  }

  $profile = $form_state['customer_profile'];
  $profile->status = TRUE;
  field_attach_submit('commerce_customer_profile', $profile, $form['customer_profile_billing'], $form_state);
  commerce_customer_profile_save($profile);
  $order_wrapper->commerce_customer_billing = $profile;

  field_attach_submit('commerce_order', $order, $form['questions'], $form_state);

  commerce_order_save($order);
  if(!empty($line_items_to_delete)) {
    commerce_line_item_delete_multiple($line_items_to_delete);
  }

  $form_state['redirect'] = 'admin/pab/bookings';
}

function pab_booking_create_event($new_product, $dates) {
  $event = bat_event_create(array(
    'type' => PAB_EVENT_TYPE,
    'start_date' => date('Y-m-d H:i:s', strtotime($dates['start_date'])),
    'end_date' => date('Y-m-d H:i:s', strtotime($dates['end_date'])),
    'uid' => 0,
    'created' => REQUEST_TIME
  ));
  $event->event_state_reference[LANGUAGE_NONE][0]['state_id'] = $new_product->status;
  $event->event_bat_unit_reference[LANGUAGE_NONE][0]['target_id'] = $new_product->unit->unit_id;
  $event->save();
  return $event;
}

function pab_booking_create_line_item($new_product, $dates) {
  $line_item = commerce_product_line_item_new($new_product->product, 1, $new_product->order_id, array(), 'product');
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $line_item_wrapper->field_start_date->set(strtotime($dates['start_date']));
  $line_item_wrapper->field_end_date->set(strtotime($dates['end_date']));
  $line_item_wrapper->field_events_reference->set($new_product->event->event_id);
  $line_item_wrapper->field_bat_type->set($new_product->type->type_id);
  $line_item_wrapper->commerce_unit_price->amount = $new_product->price;
  commerce_line_item_rebase_unit_price($line_item_wrapper->value());
  commerce_line_item_save($line_item);
  return $line_item;
}

function pab_booking_delete_form($form, &$form_state, $order) {
  $form_state['order'] = $order;
  $form['#submit'][] = 'pab_product_delete_form_submit';
  $form = confirm_form($form,
    t('Are you sure you want to delete this booking?'),
    'admin/pab/bookings',
    '',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  return $form;
}

function pab_product_delete_form_submit($form, &$form_state) {
  $order = $form_state['order'];
  pab_delete_booking($order);
  drupal_set_message(t('The booking has been deleted.'));
  watchdog('bat', 'Deleted Order %order.', array('%order' => $order->order_id));
  $form_state['redirect'] = 'admin/pab/bookings';
}

