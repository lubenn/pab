<?php

class views_handler_bat_booking_status_filter extends views_handler_filter_boolean_operator {

  function construct() {
    parent::construct();
  }

  function get_value_options() {
    $this->value_options = array(1 => t('Booked'), 0 => t('Cancelled'));
  }

}
