<?php

class views_handler_bat_booking_products extends views_handler_field {
  function render($values) {
    $booking_id = $this->get_value($values);
    $line_items = entity_metadata_wrapper('bat_booking', $booking_id)->field_line_item->value();
    $products = array();
    foreach($line_items as $line_item) {
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
      if($line_item_wrapper->type->value() == 'product') {
        $products[] = $line_item_wrapper->commerce_product->title->value();
      }
    }

    return theme('item_list', array('items' => $products));
  }
}
