<?php

/**
 * @file
 * Definition of views_handler_field_term_link_edit.
 */

/**
 * Field handler to present a term edit link.
 *
 * @ingroup views_field_handlers
 */
class views_handler_bat_product_edit extends bat_type_handler_link_field {
  /**
   * {@inheritdoc}
   */
  public function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $type = $values->{$this->aliases['type']};

    // Creating a dummy type to check access against.
    if(!user_access('administer bat products')) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $type_id = $values->{$this->aliases['type_id']};

    $options = array();
    if (!empty($this->options['destination'])) {
      $options = array('query' => drupal_get_destination());
    }

    return l($text, 'admin/pab/products/' . $type_id . '/edit', $options);
  }

}
