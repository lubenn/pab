<?php

class views_handler_bat_booking_status extends views_handler_field_boolean {

  function render($values) {
    $value = $this->get_value($values);
    if (!empty($this->options['not'])) {
      $value = !$value;
    }
    return $value ? t('Booked') : t('Cancelled');
  }
}
