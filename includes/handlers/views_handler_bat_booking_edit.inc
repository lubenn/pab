<?php

/**
 * @file
 * Definition of views_handler_field_term_link_edit.
 */

/**
 * Field handler to present a term edit link.
 *
 * @ingroup views_field_handlers
 */
class views_handler_bat_booking_edit extends views_handler_field {
  /**
   * {@inheritdoc}
   */
  public function construct() {
    parent::construct();
    $this->additional_fields['order_id'] = 'order_id';
    $this->additional_fields['uid'] = 'uid';
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    // Ensure the user has access to edit this order.
    $order = commerce_order_new();
    $order->order_id = $this->get_value($values, 'order_id');
    $order->uid = $this->get_value($values, 'uid');

    if (!commerce_order_access('update', $order)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    return l($text, 'admin/pab/bookings/' . $order->order_id . '/edit', array('query' => drupal_get_destination()));
  }

}
