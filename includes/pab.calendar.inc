<?php

function pab_calendar() {

  drupal_set_title(t('Calendar'));

  $fc_user_settings = array(
    'batCalendar' => array(
      array(
        'unitType' => 'all',
        'eventType' => PAB_EVENT_TYPE,
        'eventGranularity' => 'bat_daily',
        'validRange' => ['start' => date('Y-m-d', time())],
        'eventStartEditable' => FALSE,
        'eventDurationEditable' => FALSE,
        'editable' => FALSE,
      ),
    ),
  );

  $calendar_settings['modal_style'] = 'default';
  $calendar_settings['user_settings'] = $fc_user_settings;
  $calendar_settings['calendar_id'] = 'fullcalendar-scheduler';
  $calendar_settings['class'] = array('fixed_event_states');

  $form = drupal_get_form('pab_calendar_bulk_update_form');
  $render_array = array(
    'form' => $form,
    'calendar' => array(
      '#theme' => 'bat_fullcalendar',
      '#calendar_settings' => $calendar_settings,
    ),
  );

  return $render_array;
}


/**
 * Bulk update form.
 */
function pab_calendar_bulk_update_form($form, &$form_state) {
  $form['bulk_update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bulk update'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $types = bat_unit_get_types(NULL, TRUE);

  $types_options = array();
  $types_options['_all'] = t('All');
  foreach ($types as $type) {
    $type_bundle = bat_type_bundle_load($type->type);
    if (is_array($type_bundle->default_event_value_field_ids)) {
      if (isset($type_bundle->default_event_value_field_ids['availability']) && !empty($type_bundle->default_event_value_field_ids['availability'])) {
        $types_options[$type->type_id] = $type->name;
      }
    }
  }

  $form['bulk_update']['type'] = array(
    '#type' => 'select',
    '#title' => t('Product'),
    '#options' => $types_options,
    '#required' => TRUE,
  );

  $form['bulk_update'] += bat_date_range_fields();

  $form['bulk_update']['state'] = array(
    '#type' => 'select',
    '#title' => t('State'),
    '#options' => bat_unit_state_options('availability', array('blocking' => 0)),
    '#required' => TRUE,
  );

  $form['bulk_update']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

function pab_calendar_bulk_update_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  $start_date = new DateTime($values['bat_start_date']);
  $end_date = new DateTime($values['bat_end_date']);
  $end_date->sub(new DateInterval('PT1M'));

  $event_state = $values['state'];

  if($values['type'] == '_all') {
    $units = bat_unit_load_multiple(FALSE);
  } else {
    $type = bat_type_load($values['type']);
    $units = bat_unit_load_multiple(FALSE, array('type_id' => $type->type_id));
  }

  foreach ($units as $unit) {
    $query = db_select('bat_events', 'e');
    $query->leftJoin('field_data_event_bat_unit_reference', 'u', 'u.entity_id = e.event_id');
    $query->fields('e', array('event_id'));
    $query->condition('e.type', 'availability');
    $query->condition('u.event_bat_unit_reference_target_id', $unit->unit_id);

    $or = db_or();
    $or->condition('e.start_date', array($start_date->format('Y-m-d H:i:s'),$end_date->format('Y-m-d H:i:s')), 'BETWEEN');
    $or->condition('e.end_date', array($start_date->format('Y-m-d H:i:s'),$end_date->format('Y-m-d H:i:s')), 'BETWEEN');
    $query->condition($or);

    $results = $query->execute()->fetchAll();
    $existing_dates = array();
    if(!empty($results)) {
      foreach($results as $existing_event) {
        $existing_event = bat_event_load($existing_event->event_id);
        if($existing_event->event_state_reference[LANGUAGE_NONE][0]['state_id'] != 3) {
          continue;
        }

        form_set_error('', t('This action cannot be performed, you have bookings between these days.'));
        return;
      }
    }
  }

}

/**
 * Submit callback for bat_event_ui_bulk_update_form form.
 */
function pab_calendar_bulk_update_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $start_date = new DateTime($values['bat_start_date']);
  $end_date = new DateTime($values['bat_end_date']);
  $end_date->sub(new DateInterval('PT1M'));

  $event_state = $values['state'];
  if($values['type'] == '_all') {
    $units = bat_unit_load_multiple(FALSE, array('type' => 'product'));
  } else {
    $type = bat_type_load($values['type']);
    $units = bat_unit_load_multiple(FALSE, array('type_id' => $type->type_id));
  }

  foreach ($units as $unit) {
    $event = bat_event_create(array(
      'type' => 'availability',
      'start_date' => $start_date->format('Y-m-d H:i:s'),
      'end_date' => $end_date->format('Y-m-d H:i:s'),
      'created' => REQUEST_TIME,
    ));

    $event->event_bat_unit_reference[LANGUAGE_NONE][0]['target_id'] = $unit->unit_id;
    $event->event_state_reference[LANGUAGE_NONE][0]['state_id'] = $event_state;
    $event->save();
  }
}
