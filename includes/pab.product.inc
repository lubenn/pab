<?php

function pab_product_form($form, &$form_state, $product = FALSE) {
  $form = array();

  $default_currency = commerce_currency_load();

  $type = bat_unit_get_type_bundles(PAB_PRODUCT_TYPE);
  $commerce_product = FALSE;

  if($product) {
    $type = $product;

    $commerce_product = commerce_product_load($product->field_product[LANGUAGE_NONE][0]['product_id']);
    $form['product'] = array(
      '#type' => 'value',
      '#value' => $product
    );
  }

  field_attach_form('bat_type', $type, $form, $form_state);

  $form['field_availability']['#access'] = FALSE;
  $form['field_product']['#access'] = FALSE;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Product Name'),
    '#required' => TRUE,
    '#default_value' => isset($type->name) ? $type->name : '',
    '#description' => t('Enter a name for this product.')
  );

  $form['price'] = array(
    '#type' => 'textfield',
    '#title' => t('Price'),
    '#required' => TRUE,
    '#input_group' => TRUE,
    '#field_prefix' => '',
    '#field_suffix' => $default_currency['code'],
    '#description' => t('The price of the product.'),
    '#default_value' => $commerce_product && isset($commerce_product->commerce_price[LANGUAGE_NONE][0]['amount']) ? commerce_currency_amount_to_decimal($commerce_product->commerce_price[LANGUAGE_NONE][0]['amount'], $default_currency['code']): '',
    '#element_validate' => array('element_validate_integer_positive'),
    '#weight' => 47
  );

  $form['available'] = array(
    '#type' => 'checkbox',
    '#title' => t('Available'),
    '#default_value' => !empty($type->field_availability) && $type->field_availability[LANGUAGE_NONE][0]['state_id'] != 1 ? 0 : 1,
    '#description' => t('Is this product available to book?'),
    '#weight' => 48
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => isset($type->status) ? $type->status : 1,
    '#description' => t('Is this product shown on the site?'),
    '#weight' => 49
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 50
  );
  return $form;
}

function pab_product_form_submit($form, &$form_state) {
  global $user;

  $is_new = TRUE;
  $message = t('Product created');
  // create BAT Type
  $product = bat_type_create();
  $product->type = PAB_PRODUCT_TYPE;
  $product->uid = $user->uid;

  if(isset($form_state['values']['product'])) {
    $product = $form_state['values']['product'];
    $cp = commerce_product_load($product->field_product[LANGUAGE_NONE][0]['product_id']);
    $is_new = FALSE;
    $message = t('Product updated');
  }

  $product->name = $form_state['values']['name'];
  $product->created = REQUEST_TIME;
  $product->status = $form_state['values']['status'];

  // Save entity fields
  field_attach_submit('bat_type', $product, $form, $form_state);

  $product->save();

  $product->field_availability[LANGUAGE_NONE][0]['state_id'] = $form_state['values']['available'] ? 1 : 2;

  field_attach_update('bat_type', $product);

  if($is_new) {
    // have to create units but we only want 1?
    $wrapper = entity_metadata_wrapper('bat_type', $product);
    $type_id = $wrapper->getIdentifier();
    $unit = bat_unit_create(array('type' => PAB_PRODUCT_TYPE));
    $unit->name = $product->name;
    $unit->created = !empty($unit->date) ? strtotime($unit->date) : REQUEST_TIME;
    $unit->type_id = $type_id;
    $unit->default_state = 1;
    $unit->uid = $user->uid;
    $unit->save();

    // create a product
    // generate an sku
    $sku = preg_replace('@[^A-Z|a-z|0-9|\-|_]@', '', $product->name);
    $sku = drupal_substr($sku, 0, 255);
    $sku = drupal_strtolower($sku);
    if (!commerce_product_validate_sku_unique($sku, '')) {
      $original_sku = $sku;
      $i = 0;
      do {
        $sku = drupal_substr($original_sku, 0, (255 - drupal_strlen($i)));
        $sku = $sku . '' . $i;
        $i++;
      } while (!commerce_product_validate_sku_unique($sku, ''));
    }

    $cp = commerce_product_new('product');
    $cp->uid = $user->uid;
    $cp->sku = $sku; // need to auto sku
    $cp->status = 1; // always active?
    $cp->language = LANGUAGE_NONE;
  }

  $form_state['values']['cp'] = $cp;

  $cp->title = $product->name;
  $cp->commerce_price[LANGUAGE_NONE][0] = array(
   'amount' => $form_state['values']['price'] * 100,
   'currency_code' => 'GBP',
  );

  commerce_product_save($cp);

  if($is_new) {
    // need to reference product in bat_type
    $product->field_product[LANGUAGE_NONE][0]['product_id'] = $cp->product_id;
    field_attach_update('bat_type', $product);
  }

  drupal_set_message($message);
  $form_state['redirect'] = 'admin/pab/products';
}

function pab_product_delete_form($form, &$form_state, $type) {
  $form_state['bat_type'] = $type;
  $form['#submit'][] = 'pab_product_delete_form_submit';
  $form = confirm_form($form,
    t('Are you sure you want to delete  %name?', array('%name' => $type->name)),
    'admin/pab/products',
    '',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  return $form;
}

function pab_product_delete_form_submit($form, &$form_state) {
  $type = $form_state['bat_type'];
  bat_type_delete($type);
  drupal_set_message(t('%name has been deleted.', array('%name' => $type->name)));
  watchdog('bat', 'Deleted Type %name.', array('%name' => $type->name));
  $form_state['redirect'] = 'admin/pab/products';
}
