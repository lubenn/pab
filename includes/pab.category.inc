<?php

function pab_category_form($form, &$form_state, $term = FALSE) {
  $form = array();
  if($term) {
    $form['term'] = array(
      '#type' => 'value',
      '#value' => $term,
    );
  }
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Category Name'),
    '#required' => TRUE,
    '#default_value' => isset($term->name) ? $term->name : '',
    '#description' => t('Enter a name for this category.')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );

  return $form;
}

function pab_category_form_validate($form, &$form_state) {
  if($term = taxonomy_get_term_by_name($form_state['values']['label'], 'categories')) {
    $term = reset($term);
    if(isset($form_state['values']['term']) && $form_state['values']['term']->tid == $term->tid) {
      return;
    }
    form_set_error('label', t('This category already exists.'));
  }
}

function pab_category_form_submit($form, &$form_state) {
  $term = new stdClass();
  $message = t('Category Created');
  if(isset($form_state['values']['term'])) {
    $term = $form_state['values']['term'];
    $message = t('Category Updated');
  }
  $term->name = $form_state['values']['label'];
  $term->vid = 2; // should usually be 2 but maybe need to lookup
  taxonomy_term_save($term);

  drupal_set_message($message);

  $form_state['redirect'] = 'admin/pab/categories';
}

function pab_category_delete_form($form, &$form_state, $term) {
  $form_state['term'] = $term;
  $form['#submit'][] = 'pab_category_delete_form_submit';
  $form = confirm_form($form,
    t('Are you sure you want to delete  %name?', array('%name' => $term->name)),
    'admin/pab/categories',
    '',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  return $form;
}

function pab_category_delete_form_submit($form, &$form_state) {
  $term = $form_state['term'];
  taxonomy_term_delete($term->tid);
  drupal_set_message(t('%name has been deleted.', array('%name' => $term->name)));
  $form_state['redirect'] = 'admin/pab/categories';
}
