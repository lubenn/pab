<?php

function pab_questions_checkout_form($form, &$form_state, $checkout_pane, $order) {
  $pane_form = array('#parents' => array($checkout_pane['pane_id']));
  field_attach_form('commerce_order', $order, $pane_form, $form_state, $langcode = NULL);
  $display = FALSE;

  foreach (element_children($pane_form) as $child) {
    if(strpos($child, 'sf_') === FALSE) {
      unset($pane_form[$child]);
      continue;
    }
    $display = TRUE;
  }

  if($display) {
    return $pane_form;
  }
}

function pab_questions_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  // Do nothing unless the checkout pane still exists.
  if (!isset($form[$checkout_pane['pane_id']])) {
    return;
  }

  field_attach_submit('commerce_order', $order, $form[$checkout_pane['pane_id']], $form_state);
  commerce_order_save($order);
}
